файле addserv нужно настроить пути
ALL_HOSTS - все виртуальные хосты
DIR_SITES_ENABLED и DIR_SITES_ENABLED - папки с доступными сайтами
FILE_HOSTS - где лежит файлик hosts

по дефолту так

ALL_HOSTS="/var/www"
DIR_SITES_ENABLED="/etc/nginx/sites-enabled"
DIR_SITES_AVAILABLE="/etc/nginx/sites-available"
FILE_HOSTS="/etc/hosts"
OWNER="www-data"


Для работы нужно скопировать создать симлинк для addserv
в папку /usr/bin/ - удобнее так

sudo ln -s /path/to/addserv/addserv /usr/bin/addserv

Step 4 - Install SSL
-----------
Make sure that OpenSSL is installed on your server.

sudo apt-get install openssl

Create a new directory for the SSL certificate and generate the certificate with OpenSSL.

mkdir -p /etc/nginx/ssl
cd /etc/nginx/ssl

Generate the certificate and key file:

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/open.key -out /etc/nginx/ssl/open.crt

Change the permission of the private key to 600:

chmod 600 open.key

The SSL certificate installation is done.


listen 80;
listen 443 ssl;

ssl_certificate      /etc/nginx/ssl/open.crt;
ssl_certificate_key  /etc/nginx/ssl/open.key;

source
https://www.howtoforge.com/tutorial/install-opencart-with-nginx-and-ssl-on-ubuntu-15-10/

yii always need web as root dir
sudo addserv -5 -yii yii-basic web

