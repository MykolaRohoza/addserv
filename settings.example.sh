#!/bin/sh

ALL_HOSTS="/path/to/directory/www"
DIR_SITES_ENABLED="/etc/nginx/sites-enabled"
DIR_SITES_AVAILABLE="/etc/nginx/sites-available"
FILE_HOSTS="/etc/hosts"

user="www-data"
group="www-data"

SSL_CERTIFICATE="/etc/nginx/ssl/open.crt"
SSL_CERTIFICATE_KEY="/etc/nginx/ssl/open.key"
